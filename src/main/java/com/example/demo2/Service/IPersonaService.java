package com.example.demo2.Service;

import com.example.demo2.Entity.Persona;
import org.springframework.stereotype.Service;

import java.util.List;

public interface IPersonaService {

    public List<Persona> listarPersonas();
    public Persona buscarPersona(Long id);
    public Persona registrarPersona(Persona persona);
    public Persona modificarPersona(Long id, Persona persona);
    public void eliminarPersona(Long id);
}
