package com.example.demo2.Controller;

import com.example.demo2.Entity.Persona;
import com.example.demo2.Service.IPersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/personas")
public class PersonaController {

    @Autowired
    private IPersonaService personaService;

    @GetMapping("/listar")
    public List<Persona> listarPersonas() {
        return personaService.listarPersonas();
    }

    @GetMapping("/buscar/{id}")
    public Persona buscarPersona(@PathVariable("id") Long idPersona) {
        return personaService.buscarPersona(idPersona);
    }

    @PostMapping("/registrar")
    public ResponseEntity<Persona> registrarPersona(@RequestHeader HttpHeaders headers, @RequestBody Persona persona){
        if (headers.get("token").get(0).equals("12345")) {
            System.out.println(headers);
            personaService.registrarPersona(persona);
            return new ResponseEntity<>(persona, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(persona, HttpStatus.UNAUTHORIZED);
        }
    }

    @PutMapping("/actualizar/{id}")
    public Persona actualizarPersona(@PathVariable("id") Long idPersona, @RequestBody Persona persona) {
        return personaService.modificarPersona(idPersona,persona);
    }

    @DeleteMapping("/eliminar/{id}")
    public void eliminarPersona(@PathVariable("id") long idPersona) {
       personaService.eliminarPersona(idPersona);
    }


}
